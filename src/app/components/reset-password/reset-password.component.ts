import { Component, isDevMode, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "src/environments/environment";
@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"]
})
export class ResetPasswordComponent implements OnInit {
  user: any;
  accessToken: any;
  activeFieldId: string = "";
  service_url = environment.service_url;
  login_url = environment.login_url;
  translate;
  username;
  constructor(
    translate: TranslateService,
    private http: HttpClient,
    private router: Router
  ) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang("ar");
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    // translate.use("ar");
  }

  ngOnInit() {
  //  this.getToken();
    this.username = this.user.username || "Hesham";
  }

  

  changeLang(lang) {
    localStorage.removeItem("core_lang");
    localStorage.setItem("core_lang", JSON.stringify({ lang: lang }));
    this.translate.use(lang);
  }

  onLogout() {
    document.cookie = "kindix-token=;";
    window.location.href = this.user.logout_url;
  }

  getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2)
      return parts
        .pop()
        .split(";")
        .shift();
  }

  onFocus(fieldId): void {
    this.activeFieldId = fieldId;
  }
  onFocusout(fieldId): void {
    this.activeFieldId = fieldId;
  }

  forgetPassword(params: {}) {
    console.log(params);
    const headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return this.http.post(
      this.service_url +
        "accounts/reset_password",
      JSON.stringify(params),
      { headers: headers }
    );
  }
  onSubmit(form) {
    form.value["email"] = localStorage.getItem("email");
    console.log(form.value);
    this.forgetPassword(form.value).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log({ err });
      }
    );
  }
}
