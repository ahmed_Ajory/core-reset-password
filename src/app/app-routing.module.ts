import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ForgetPasswordComponent } from "./components/forget-password/forget-password.component";
import { ResetPasswordComponent } from "./components/reset-password/reset-password.component";

const routes: Routes = [
  { path: "", redirectTo: "/forget_password", pathMatch: "full" },
  { path: "forget_password", component: ForgetPasswordComponent },
  { path: "reset_password", component: ResetPasswordComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
